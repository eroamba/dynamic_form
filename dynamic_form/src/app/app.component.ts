import { Component } from '@angular/core';
import { ROLE_FORM_FIELDS } from './form-data/role';
import { MEMBER_FORM_FIELDS } from './form-data/member';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'dynamic_form';
  formDataRoles: any[] = ROLE_FORM_FIELDS;
  formDataMembers: any[] = MEMBER_FORM_FIELDS;

  fbGroupRole:FormGroup=new FormGroup({});
  fbGroupMember:FormGroup=new FormGroup({});


  
  constructor(private fb: FormBuilder) {
    this.fbGroupRole= this.fb.group(this.createRoleFormControls());
    this.fbGroupMember= this.fb.group(this.createMemberFormControls());
  }



  createRoleFormControls() {
    const formControls: any = {};

    this.formDataRoles.forEach(control => {
      let validators: any[] = [];

      if (control.validations) {
        control.validations.forEach((validation:any )=> {
          if (validation.validator === 'required') {
            validators.push(Validators.required);
          } else if (validation.validator === 'email') {
            validators.push(Validators.email);
          }
        });
      }

      // Définition de la valeur par défaut du contrôle
      const defaultValue = control.defaultValue !== undefined ? control.defaultValue : '';

      // Création du contrôle avec sa valeur par défaut et ses validateurs
      formControls[control.formControlName] = [
        defaultValue,
        validators
      ];
    
    });

    return formControls;
  }

  createMemberFormControls() {
    const formControls: any = {};

    this.formDataMembers.forEach(control => {
      let validators: any[] = [];

      if (control.validations) {
        control.validations.forEach((validation:any )=> {
          if (validation.validator === 'required') {
            validators.push(Validators.required);
          } else if (validation.validator === 'email') {
            validators.push(Validators.email);
          }
        });
      }

      // Définition de la valeur par défaut du contrôle
      const defaultValue = control.defaultValue !== undefined ? control.defaultValue : '';

      // Création du contrôle avec sa valeur par défaut et ses validateurs
      formControls[control.formControlName] = [
        defaultValue,
        validators
      ];
    
    });

    return formControls;
  }

  saveRole() {
    // Afficher les valeurs du formulaire de rôle
    alert(JSON.stringify(this.fbGroupRole.value));
  }

  saveMember() {
    // Afficher les valeurs du formulaire de membre
    alert(JSON.stringify(this.fbGroupMember.value));
  }


}
