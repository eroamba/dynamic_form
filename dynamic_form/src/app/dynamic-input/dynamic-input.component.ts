import { CommonModule } from '@angular/common';
import { Component, Input, Optional, Self } from '@angular/core';
import { ControlContainer, ControlValueAccessor, FormGroup, FormGroupDirective, FormsModule, NgControl, ReactiveFormsModule } from '@angular/forms';

export const NOOP_VALUE_ACCESSOR: ControlValueAccessor =
{
  writeValue(): void { },
  registerOnChange(): void { },
  registerOnTouched(): void { }
};

@Component({
  selector: 'app-dynamic-input',
  standalone: true,
  templateUrl: './dynamic-input.component.html',
  styleUrls: ['./dynamic-input.component.css'],
  imports: [CommonModule, FormsModule, ReactiveFormsModule], 
  viewProviders: [{
    provide: ControlContainer,
    useExisting: FormGroupDirective
  }]
})
export class DynamicInputComponent {
  @Input() checkedValue: boolean = true
  @Input() data: any[]=[]
  @Input() dynamicForm: FormGroup=new FormGroup({});


  constructor(@Self() @Optional() public ngControl: NgControl) {
    if (this.ngControl) {
      this.ngControl.valueAccessor = NOOP_VALUE_ACCESSOR;
    }
  }



  getErrorMessage(control: { name: any; validations: any; }) {
    const formControl = this.dynamicForm.get(control.name);

    if (formControl) { 
      for (let validation of control.validations) {
        if (formControl.hasError(validation.name)) {
          return validation.message;
        }
      }
    }
  
    return '';
  }

}
