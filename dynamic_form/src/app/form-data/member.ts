export const MEMBER_FORM_FIELDS = [
  {
    "type": "select",
    "id": "civilite",
    "label": "Civilité",
    "formControlName": "civilite",
    "classContainer": "col-4",
    "isKeyValue": true,
    "options": [
      { "name": "Monsieur", "id": "monsieur" },
      { "name": "Madame", "id": "madame" },
      { "name": "Mademoiselle", "id": "mademoiselle" }
    ],
    "placeholder": "Veuillez sélectionner la civilité",
    "defaultValue": "",
    "validations": []
  },

  {
    "type": "text",
    "id": "last_name",
    "label": "Nom de famille",
    "placeholder": "Entrez le nom de famille",
    "formControlName": "last_name",
    "classContainer": "col-sm-4",
    "defaultValue": "",
    "validations": [
      {
        "name": "required",
        "validator": "required",
        "message": "Le nom de famille est requis"
      }
    ]
  },
  {
    "type": "text",
    "id": "first_name",
    "label": "Prénoms",
    "placeholder": "Entrez les prénoms",
    "formControlName": "first_name",
    "classContainer": "col-sm-4",
    "defaultValue": "",
    "validations": [
      {
        "name": "required",
        "validator": "required",
        "message": "Le prénom est requis"
      }
    ]
  },
  {
    "type": "email",
    "id": "email",
    "label": "Email",
    "placeholder": "Entrez l'email",
    "formControlName": "email",
    "classContainer": "col-sm-6",
    "defaultValue": "",
    "validations": [
      {
        "name": "required",
        "validator": "required",
        "message": "L'email est requis"
      },
      {
        "name": "email",
        "validator": "email",
        "message": "L'email est invalide"
      }
    ]
  },
  {
    "type": "text",
    "id": "phone",
    "label": "Téléphone",
    "placeholder": "Entrez le numéro de téléphone",
    "formControlName": "phone",
    "classContainer": "col-sm-6",
    "defaultValue": "",
    "validations": [
      {
        "name": "required",
        "validator": "required",
        "message": "Le numéro de téléphone est requis"
      }
    ]
  },
  {
    "type": "select",
    "id": "status",
    "label": "Statut",
    "formControlName": "status",
    "classContainer": "col-6",
    "isKeyValue": true,
    "options": [
      { "name": "Actif", "id": "actif" },
      { "name": "Inactif", "id": "inactif" }
    ],
    "placeholder": "Veuillez sélectionner le statut",
    "defaultValue": "",
    "validations": []
  },

  {
    "type": "select",
    "id": "user_type",
    "label": "Type d'utilisateur",
    "formControlName": "user_type",
    "classContainer": "col-6",
    "isKeyValue": true,
    "options": [
      { "name": "Utilisateur régulier", "id": "regular" },
      { "name": "Administrateur", "id": "admin" }
    ],
    "placeholder": "Veuillez sélectionner le type d'utilisateur",
    "defaultValue": "",
    "validations": []
  }
]
