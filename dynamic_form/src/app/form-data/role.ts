export const ROLE_FORM_FIELDS=[
    {
      type: "text",
      id: "name",
      label: "nom",
      placeholder: "Saisissez le nom",
      formControlName: "name",
      classContainer: "col-sm-12",
      defaultValue:"",
      validations: [
        {
          name: "required",
          validator: "required",
          message: "Name is required"
        }
      ]
    },
    {
      type: "textarea",
      id: "description",
      label: "Description",
      placeholder: "Saisissez la description",
      formControlName: "description",
      classContainer: "col-sm-12",
      defaultValue:"",
      validations: [
        {
          name: "required",
          validator: "required",
          message: "Description is required"
        }
      ]
    },
  ];
  